#include <stdio.h>
#include<stdlib.h>
#include <math.h>
#include "MersenneTwister.h" 

//Question 1: Computing PI with the Monte Carlo method
/* ------------------------------------------------------------------------------------- */
/* simPi - Estimate π using the Monte Carlo method with a given number of points         */
/*                                                                                       */
/* Input: numPoints - The number of random points to generate and use in the estimation  */
/*                                                                                       */
/* Output: The estimated value of π based on the ratio of points inside a quarter circle */
/* ------------------------------------------------------------------------------------- */

double simPi(int numPoints)
{
    double xr,
           yr;

    // Initialize a counter for points inside the circle
    int sum=0;
    
    for (int i=0;i<numPoints;i++)
    {
        // Generate random coordinates within a unit square
        xr=genrand_real1();
        yr=genrand_real1();

        // Check if the point is inside the quarter circle (radius 1)
        if (xr*xr+yr*yr<1)
        {
            sum=sum+1;
        }
    }

    // Calculate the estimated value of π based on the ratio of points inside the circle
    return (double)(sum)*4/numPoints;
}

//Question 2: Calculating the mean for independent experiments
/* ------------------------------------------------------------------------------------------ */
/* N_simPi - Compute independent experiments and obtain the mean of π estimates               */
/*                                                                                            */
/* Input: numDrawings - The number of random points to generate for each experiment           */
/*        numReplicates - The number of independent experiments to perform                    */
/*        results - A pointer to an array to store the estimated π values for each experiment */
/*                                                                                            */
/* Output: The mean of the estimated π values, absolute error, and relative error             */
/* ------------------------------------------------------------------------------------------ */

double N_simPi(int numDrawings,int numReplicates,double *results)
{
    double meanPi=0.0,
           absoulteError,
           relativeError;
        
    // Simulate the experiments
    for (int i=0;i<numReplicates;++i)
    {
        results[i] = simPi(numDrawings);
        meanPi+=results[i];
    }

    // Calculate the arithmetic mean
    meanPi=meanPi/numReplicates;

    // Calculate the absolute error by subtracting the estimated π from the actual π (M_PI)
    absoulteError=fabs(meanPi-M_PI);
    //printf("The absolute error :%f\n",absoulteError);

    // Calculate the relative error by dividing the estimated π by the actual π (M_PI)
    relativeError=absoulteError/M_PI;
    //printf("The relative error :%f\n",relativeError);
    
    return meanPi;
}

//Question 3:Computing of confidence intervals around the simulated mean
/* ------------------------------------------------------------------------------------------------------------ */
/* getCriticalValue - Get the critical value from a table of pre-established values                             */
/*                                                                                                              */
/* Input: alpha - The significance level or confidence level                                                    */
/*        freedomdegree - The degree of freedom associated with the distribution                                */
/*                                                                                                              */
/* Output: The critical value associated with the significance level alpha and degree of freedom freedomdegree  */
/* ------------------------------------------------------------------------------------------------------------ */

double getCriticalValue(double alpha,int freedomdegree)
{
    // Tableau de valeurs critiques
     double elements[] =
     { 
        63.6551, 9.9247, 5.8408, 4.6041, 4.0322, 3.7074, 3.4995, 3.3554, 3.2498, 3.1693,
        3.1058, 3.0545, 3.0123, 2.9768, 2.9467, 2.9208, 2.8983, 2.8784, 2.8609, 2.8454,
        2.8314, 2.8188, 2.8073, 2.797, 2.7874, 2.7787, 2.7707, 2.7633, 2.7564, 2.75,
        2.744, 2.7385, 2.7333, 2.7284, 2.7238, 2.7195, 2.7154, 2.7115, 2.7079, 2.7045,
        2.7012, 2.6981, 2.6951, 2.6923, 2.6896, 2.687, 2.6846, 2.6822, 2.68, 2.6778,
        2.6757, 2.6737, 2.6718, 2.67, 2.6682, 2.6665, 2.6649, 2.6633, 2.6618, 2.6603,
        2.6589, 2.6575, 2.6561, 2.6549, 2.6536, 2.6524, 2.6512, 2.6501, 2.649, 2.6479,
        2.6468, 2.6459, 2.6449, 2.6439, 2.643, 2.6421, 2.6412, 2.6404, 2.6395, 2.6387,
        2.6379, 2.6371, 2.6364, 2.6356, 2.6349, 2.6342, 2.6335, 2.6328, 2.6322, 2.6316,
        2.6309, 2.6303, 2.6297, 2.6292, 2.6286, 2.628, 2.6275, 2.6269, 2.6264, 2.6259,
        2.6254, 2.6249, 2.6244, 2.624, 2.6235, 2.623, 2.6225, 2.6221, 2.6217, 2.6212,
        2.6208, 2.6204, 2.62, 2.6196, 2.6192, 2.6189, 2.6185, 2.6181, 2.6178, 2.6174
    };

    return elements[freedomdegree];
}

/* ------------------------------------------------------------------------------------------------------------ */
/*  confidenceInterval - Calculate confidence intervals around the simulated mean                               */
/*                                                                                                              */
/* Input: alpha - The significance level or confidence level                                                    */
/*        numReplicates - The number of independent experiments or replicates                                   */
/*        numDrawings - The number of random points to generate for each experiment                             */
/* ------------------------------------------------------------------------------------------------------------ */

void confidenceInterval(int numReplicates, int numDrawings, double alpha)
{
    double upperLimit,
           lowerLimit,
           errorMargin,
           meanPi,
           t_critical,
           standardDeviation=0.0;
    double *results = (double *)malloc(numReplicates * sizeof(double));

    // Calculate the mean of π estimates for numReplicates experiments
    meanPi=N_simPi(numDrawings,numReplicates,results);

    // Calculate the standard deviation 
    for (int j = 0; j < numReplicates; j++) 
    {
        standardDeviation +=(results[j] - meanPi) * (results[j] - meanPi);
    }
    standardDeviation=sqrt(standardDeviation/ (numReplicates - 1));

    // Calculate the t-critical value from a table for a given alpha and degree of freedom
    t_critical = getCriticalValue(0.01, numReplicates - 1); 

    // Calculate the margin of error for the confidence interval
    errorMargin = t_critical * standardDeviation;

    // Confidence interval limits
    lowerLimit = meanPi - errorMargin;
    upperLimit = meanPi + errorMargin;

    printf("Confidence Interval: [%lf, %lf]\n", lowerLimit, upperLimit);
    free(results);
}

int main(void)
{
    double *results = (double *)malloc(50 * sizeof(double));

    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    printf("\033[0;31mComputing PI with the Monte Carlo method:\033[0m\n\n");
    printf("\033[0;32m1000 Drawings:\033[0m\n");
    printf("Pi=%f\n\n",simPi(1000));
    printf("\033[0;32m1000000 Drawings:\033[0m\n");
    printf("Pi=%f\n\n",simPi(1000000));
    printf("\033[0;32m1000000000 Drawings:\033[0m\n");
    printf("Pi=%f\n\n",simPi(1000000000));
    
    printf("\033[0;31mCalculating the mean for independent experiments:\033[0m\n\n");
    printf("\033[0;34m10 experiments:\033[0m\n");
    printf("\033[0;32m1000 Drawings:\033[0m\n");
    printf("meanPi=%f\n\n",N_simPi(1000,10,results));
    printf("\033[0;32m1000000 Drawings:\033[0m\n");
    printf("meanPi=%f\n\n",N_simPi(1000000,10,results));
    printf("\033[0;32m1000000000 Drawings:\033[0m\n");
    printf("meanPi=%f\n\n",N_simPi(1000000000,10,results));
 
    printf("\033[0;34m40 experiments:\033[0m\n");
    printf("\033[0;32m1000 Drawings:\033[0m\n");
    printf("meanPi=%f\n\n",N_simPi(1000,10,results));
    printf("\033[0;32m1000000 Drawings:\033[0m\n");
    printf("meanPi=%f\n\n",N_simPi(1000000,10,results));
    printf("\033[0;32m1000000000 Drawings:\033[0m\n");
    printf("meanPi=%f\n\n",N_simPi(1000000000,10,results));
    
    printf("\033[0;31mComputing of confidence intervals around the simulated mean:\033[0m\n\n");
    printf("\033[0;34m10 experiments:\033[0m\n");
    printf("\033[0;32m1000 Drawings:\033[0m\n");
    confidenceInterval(10,1000,0.01);
    printf("\033[0;32m1000000 Drawings:\033[0m\n");
    confidenceInterval(10,1000000,0.01);
    printf("\033[0;32m1000000000 Drawings:\033[0m\n");
    confidenceInterval(10,1000000000,0.01);
    
    printf("\033[0;34m40 experiments:\033[0m\n");
    printf("\033[0;32m1000 Drawings:\033[0m\n");
    confidenceInterval(40,1000,0.01);
    printf("\033[0;32m1000000 Drawings:\033[0m\n");
    confidenceInterval(40,1000000,0.01);
    printf("\033[0;32m1000000000 Drawings:\033[0m\n");
    confidenceInterval(40,1000000000,0.01);
    
    free(results);
}