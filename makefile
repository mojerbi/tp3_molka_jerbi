# Compiler
CC = gcc

# Compiler flags
CFLAGS = -Wall -Wextra 

# Executable name
TARGET = tp3

# Source files
SRC = MersenneTwister.c main.c  

# Header files
HEADER = MersenneTwister.h

# Object files
OBJ = $(SRC:.c=.o)

# Build the executable
$(TARGET): $(OBJ)
	$(CC) -o $@ $^ -lm


# Compile source files to object files
%.o: %.c $(HEADER)
	$(CC) $(CFLAGS) -c -o $@ $<

# Clean up the project
clean:
	rm -f $(OBJ) $(TARGET)

.PHONY: clean
